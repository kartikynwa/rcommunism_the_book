#!/usr/bin/env python3

# requires praw; install from pip
import praw
from links import *
from credentials import *

reddit = praw.Reddit(client_id=client_id,
                     client_secret=client_secret,
                     user_agent='<Linux>Scrapper by /u/YouNeverWalkAlone')

submission = reddit.submission(url='https://www.reddit.com/user/flesh_eating_turtle/comments/f2lvcp/the_most_common_antisocialist_myths_a_response/')
print(submission.selftext)

# for i, chapter in enumerate(links):
#     for j, link in enumerate(chapter):
#         f = open("{:02d}_{:02d}.md".format(i+1, j+1), "w")
#         content = reddit.submission(url=link).selftext
#         f.write(content)
#         f.close()
