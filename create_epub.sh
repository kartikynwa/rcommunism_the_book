#!/usr/bin/env sh
pandoc -o output/rcommunism_the_book.epub \
  --epub-cover-image=./cover/cover.png \
  metadata.yaml scrapped_posts/01/*.md
