Hello everybody, this was originally part of a much bigger effort post on the situation in Xinjiang. I've not been able to work on it as much as I would like and seeing the level and viciousness of propaganda being pushed on reddit about this subject makes me feel like this might be a useful touchstone for people. 

Basically what this inventory illustrates is what I found going through the primary sources for these claims. I found the research methodology of all the reports to be extremely suspect and frequently taking huge unfounded leaps (for example inventing 1120 buildings out of thin air for no discernible reason on no discernible basis).

We see that the validity of these claims boils down to literally a couple of documents which are allegedly "leaked" to dissident organisations. Organisations which themselves have been shown to lack credibility very often (unfortunately this outside of the purview of this post, and part of the original effortpost which I'm still writing). 

Here is the generally agreed upon inventory of evidence as per QZ and chinafile:

https://qz.com/1599393/how-researchers-estimate-1-million-uyghurs-are-detained-in-xinjiang/

http://www.chinafile.com/reporting-opinion/features/where-did-one-million-figure-detentions-xinjiangs-camps-come

1.	RFA reports 120,000 Uyghurs detained for showing signs of extremism. https://www.rfa.org/english/news/uyghur/detentions-01222018171657.html

 “The security chief of Kashgar city’s Chasa township recently told RFA on condition of anonymity that “approximately 120,000” Uyghurs are being held throughout the prefecture, based on information he has received from other area officials.”


2.	CHRD estimates **BASED ON 8 INTERVIEWS** that 240,000 in Kashgar, and 660,000 in Southern Xinjiang by taking an estimated 10% as per the 8 interviews used. (8 interviews out of allegedly dozens they conducted) Furthermore they get to the 550,000 / 1.3 million number by applying a 20% (from the same 8 interviews) of people going to day/evening courses which are not centers of any kind.

 https://www.nchrd.org/2018/08/china-massive-numbers-of-uyghurs-other-ethnic-minorities-forced-into-re-education-programs/


3.	Adrian Zenz, citing reports by RFA, details in his publications the genesis of the current extremist reformation program. For example, in 2014 a reeducation program targeting problematic people engaged 5000 persons to reeducation training according to gov sources. The 4 groups divided from most problematic to least received 20 days, 15, 7, and 4 days training respectively. Seizing on a government statement in which government officials spoke on rural Xinjiang peoples saying “70% simply change with surrounding, 30% have been polluted by extremist thought, and a small minority are hardened criminals”, Zenz extrapolates that this statement has become official policy, referencing Radio Free Asia and inferring that these comments have become fixed detainment quotas although besides the RFA report no such evidence is provided. Zenz provides evidence for 78 bids for construction of varying natures, some containing supermarkets and hospitals, some with features suitable for detainment; apparently none are particularly uniform in nature. Zenz details some information on budgetary spending that is publicly available. He then draws on the satellite pictures of Shawn Zhang to confirm the existence of the construction of two facilities. (not detailing the nature of these facilities in any meaningful way) **Now we get to the interesting bit, Zenz’s estimation of current detainees.** **a)** An Uyghur exile media association in Istanbul with a document “leaked from a reliable source” saying ~700,000 detainees in 27 counties lining up with the 12.3% of adult muslims in mid February 2018. The same document alleges in 68 Xinjiang counties ~900,000 in spring 2018. This document is called (Mizutani 2018) the document: https://www.newsweekjapan.jp/stories/world/2018/03/89-3_1.php **b)** The same RFA report with the anonymous head of security of Chasa township. This time though, Zenz reports it as 32,000 in Kashgar city, 10.4% of the muslim population. He references another RFA report in another county as allegedly having a 10% mandate. He goes on to say, paraphrasing, (Of course this estimate is predicated on the supposed validity of the source of the leaked document he adds, before mentioning more RFA articles which anecdotally confirm the poor conditions etc. **c)**This is where it goes off the wall for me. *After analyzing 78 bids for construction, and two satellite validations that things are built, Zenz proceeds to say, given Xinjiang’s size, it is reasonable to assume 1200 facilities in the reeducation network exist, each hosting 250- 800 people.* The sole evidence for this is his comparison to another Chinese program which was “reeducation through labour”. The mechanism for this assumption is not present. He works backwards from total interment estimates to reach the estimated interned in each facility. As far as I can tell, this extrapolation is complete fantasy. He literally invents 1120 building from thin air, with no real evidence whatsoever. https://www.academia.edu/37353916/NEW_Sept_2018_Thoroughly_Reforming_Them_Towards_a_Healthy_Heart_Attitude_-_Chinas_Political_Re-Education_Campaign_in_Xinjiang



4. The media runs wild with these reports, falsely attributing to the UN the words of an independent panel. 
https://www.reuters.com/article/us-china-rights-un/u-n-says-it-has-credible-reports-that-china-holds-million-uighurs-in-secret-camps-idUSKBN1KV1SU
 As reported thoroughly, and debunked by https://thegrayzone.com/2018/08/23/un-did-not-report-china-internment-camps-uighur-muslims/ This UN independent panel report (the person raising the concern had no expertise in the subject) based their allegations on the CHRD report. A report which was an extrapolation of the phone interviews OF EIGHT PEOPLE.


5. Australian group documents 28 compounds. They cite the completely fabricated 1200 number by Zenz. https://www.aspi.org.au/report/mapping-xinjiangs-re-education-camps


6. Agence France Presse estimate 181 facilities although how they estimated this is unknown. https://www.yahoo.com/news/inside-chinas-internment-camps-tear-gas-tasers-textbooks-052736783.html


7. Shawn Zhang has currently posted 94 facilities he believes are reeducation centers. https://medium.com/@shawnwzhang/list-of-re-education-camps-in-xinjiang-%E6%96%B0%E7%96%86%E5%86%8D%E6%95%99%E8%82%B2%E9%9B%86%E4%B8%AD%E8%90%A5%E5%88%97%E8%A1%A8-99720372419c


8. A US state department estimate that is even Higher at 3 million, which Zenz himself is incredulous of. https://www.theguardian.com/world/2019/may/04/us-accuses-china-of-using-concentration-camps-uighur-muslim-minority?CMP=share_btn_tw 

He believes they are combining numbers of people estimated to have been interned with the estimated day/evening school group.  “US defense department says that China runs 'concentration camps' in Xinjiang that may contain up to 3 million! To be honest, without citing specific new evidence, I find such statements to be overly sensationalist and speculative.”

https://twitter.com/adrianzenz/status/1124661978729930752?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1124661978729930752&ref_url=https%3A%2F%2Fqz.com%2F1599393%2Fhow-researchers-estimate-1-million-uyghurs-are-detained-in-xinjiang%2F


**So what’s the total inventory of credible information?**

1.	Interview of 8 people by the CHRD organization extrapolated to the entire population of Xinxiang.

2.	A leaked document provided to Radio Free Asia by the Head of Security of Kashgar city’s Chasa Township on the condition of anonymity. Weirdly his position, location, etc are prominent details. The document is impossible to verify.


3.	Another leaked document from a Uyghur Exile media organization of Istanbul leaked from an anonymous source. This is one of the sources Zenz uses for his 10% estimate.


4.	The existence of buildings in Xinjiang. From the 94 alleged by Zhang, 181 by AFP, or the 1200 pulled out of thin air by Zenz himself. “Researchers” then use a scandalous estimate of 1 person per some quantity square meter as a basis for this estimation which was provided by a Radio Free Asia report. 


5.	Multiple Radio Free Asia reports of “cold called officials” 4 who allegedly stated they had a 10% mandate.


That’s right folks.  Leaked documents from an exile organization/RFA, the interview of 8 Uyghurs extrapolated to the total population, satellite images of buildings, and RFA/separatist organisation reports. The entirety of evidence for this whole thing is American soft power organizations and their network of affiliates. It is then filtered through a “researcher”, theologian Adrien Zenz, whose main estimate is essentially linked to the supposed veracity of these “leaked documents”. This creates a distance between the partisan organizations putting this information forward and lends their conclusions an appearance of being independently verified.

Nowhere in the leaked documents does the number 1 million appear. The group of researchers never go in depth as to whether they mean “1 million people are currently interned”, or rather, “In total, 1 million people have in some way attended some form of rehabilitation activity, from the schools, to evening classes etc, up to this point, although we know not how many are currently in these schools.”


It is abundantly clear that the phenomenon is not a uniform policy of incarceration for incarcerations sake. The researchers themselves acknowledge this. The publicly available information from government sources, testimonials, and the researchers themselves make clear that the programs deployed can be anywhere from a few days to a few months depending on their nature. None of this nuance has been captured by western media reports on the subject.
