a summary of developments one month post-coup. **Specifically I've attempted to investigate the character and extent of fascist paramilitary violence, as this has been a particular media blackout.** Sources will be appended. The app crashes when I embed text links. **tl;dr bold text for the goods**

## CLASS WAR AND THE REGIONAL SITUATION

In Latin America, neoliberalism is understood as a failed model, austerity vividly lived, popularly resisted and rejected. The **social democratic projects of Latin America clawed some independence from yankee imperialism and neocolonialism.** **The movement may be in political retreat, but the social sectors are not defeated.** Witness mass mobilisations in Colombia, Ecuador, Chile and Argentina. 

The illusory facade of democracy is now enacted in Bolivia, a hollow fig leaf for neoliberalism. **Interim govt maintains outward rhetorical democratic facade, inverts internal reality.** Popular movements pose a legitimate threat to the interest of national and international bourgeois and ruling classes. **Bolivia's crisis of terror, of coup, of political and media repression, of deadly security forces actions is a crisis of capitalism, the failure of bourgeois democratic structures and norms, and a corresponding fascist reaction.**

In Bolivia **latent fascist reaction takes the form of the paramilitary separatist Union Juvenil Crucenista (UJC) and Civic Comites.** These are violent shock troops, complicit in the dictatorship era and emboldened by the coup. UJC acted as coup vanguard; sacking and assaulting MAS government buildings, officials, media and homes. The **horrible assault of Patricia Arce, the mayor of Vinto** has been widely shared on social media, but the overall state of UJC and allied rightist gang violence (**motoqueros**)  is both underreported or intentionally blacked out.

**A class contradiction**: the Process of Change lifted two million people out of poverty into the middle classes. These classes have solidified the mestizo character of the cities. These **newly arrived middle classes mark a turn to social fascism. Their minds remain colonized, they identify with the racist right** and disdain the communities from which they came. 

The Bolivia crisis can be understood as an **undeclared civil war,** the cities in contest with the plains. The plains are composed of poorer classes, largely miners from the provinces. Mobilisation lags in dispersed provinces. The cities rapidly mobilise. They feel newly empowered and in turn provide ranks and legitimacy to the latent fascist sector, restive but marginalized under the Morales era. 

## PARA ACTIVITY

The **Argentinian journalist Sebastian Moro disappears on the 9th Nov. Dies on the 16th Dec.** Official cause of death: "ischemic stroke". Moro's body shows signs of assault. Ongoing reports of assault and kidnapping. Media threats at gunpoint. The **brother of Senate expresident Victor Borda, brutally assaulted, tortured, currently hospitalized.**

**Union Juvenil Crucenista (UJC)  expands from its Santa Cruz base to seven major cities. Planned natl conference; to consolidate strategy and tactical plan in advance of Ley de Garantias.** Threatens to take streets if passed. Evidence of pre-coup coordination: UJC create new instagram and twitter accounts 17th October. UJC occupies the Office of the Human Rights Ombudsman. Ranks form cordons and interfere with the movement and work of Office employees. Ombudsman Nelson Cox accuses police and the AG of abrogation of duties w respect to law. "They have carried out explosions of firecrackers in my home, they have accused me of committing illicit acts, of drug traffickers, murderers, terrorists (…), they have made threats against my daughters and my family”.

**Branko Marinkovic returns from eleven year exile to Santa Cruz.** The same day Partido Demócrata Cristiano announces his candidacy for governor. Marinkovic is an [failed] 2008 coup author, Santa Cruz separatist and ultrarightist w suspected family ties to Ustashe. He is Fernando Macho Camacho's godfather.

## ONGOING POLITICAL DEVELOPMENT

**35 dead and 600-700 wounded. Estimates of 800-1200 arbitrary detained.** Frenetic ongoing political persecution. **Numbers of MAS members, militants and Union leaders in internal or external exile or arrested, or in hiding unknown.** Nine ministers granted political exile in MX embassy are denied safe passage out of country. On Arturo Murillos enemies list.

**Morales departs Mexico and arrives Arg 12th Dec.** **Arrest warrant issued by unnerved interim govt on 18th Dec. Charges: sedition, terrorism.** **Balthazar Garzon will act to invalidate arrest warrant.** Garzon is the famous Spanish lawyer who arrested and prosecuted Pinochet. MAS nationwide reunion in Cochabamba this past Sat. Delegates energized by Morales in Argentina. Optimistic assessment wrt to MAS consolidation unity and strength, despite interm gov pronouncements to contrary. Delegates elect Morales to Director of National Election Campaign. General and troubling **uncertainty wrt to proposed election. 'tentative' Aug date.** deferred elex a move to marginalise party, interim govt to entrench legitimacy. Movement calls for regional bodies to monitor and implement rapid and transparent election process.

**Ley de Garantias proposed by Legislature to protect the rights of movement members, militants and Union leaders against politically motivated persecution.** Proposal is backed by international monitors, **stalled by rightists and interim govt.** Anez proposes exemptions to Ley: no protections guaranteed to those charged of 'sedition and terrorism'. No impartial legal/judiciary mechanisms proposed to determine charges and/or to issue warrants. Chapare autonomous zone. Morales stronghold. Police expelled. Stations sacked. Police voodoo dolls strung up. **Murillo threatens to disenfranchise autonomous Chapare.**

Leaked audio: Camacho horsetrades payoffs and political favors with Marco Pumari. Interim united front crumbling. **Coup co-conspirators are "eating each other alive".** 

**Murillo forms an antiterrorist unit to root out 'foreigners' committing 'aggressive' acts** (in response to CIDH visit and findings). Media blackout. **All 53 indigenous radio stations have been suspended off air.** Telesur in-country transmissions suspended. MAS official media expropriated by interim govt. **Bolivia marks one month since Senkata massacre.**


## HUMAN RIGHTS SITUATION

45 person solidarity **delegation from Arg interrogated under duress for hours upon arrival. Mob harassement, violence and intimidation.** **Murillo: “Be careful, we are watching."**

 **"A Pandoras Box of racism." An atmosphere of general terror.** **Findings:** Desaparecidos, blacklist, political hunting, sacking, threats against families, Senkata and Cocha massacres, hostile press repression, **liberated UJC zones**, violent bodily assault, excessive force contra demonstrations. 'The delegation was unable to carry out all the activities planned because of the explicit threats made by (de facto) Government Minister Arturo Murillo and the actions of civilian shock groups."

CIDH [Comision Interamericana de Derechos Humanos] collects testimonies "from a hundred people in a safe location in the town of El Alto, private homes of other victims were visited, hospitalized people were visited and meetings were held in different parts of La Paz with political actors and urban, peasant and indigenous social movements. CIDH declares no guarantee of impartial independent judiciary.

In the wake of CIDH findings: **How much is the life of an indigenous person worth?** 'Supreme Decree 4100' proposed compensation to victim's families (Cochabamba and Senkata massacres) **$50,000 bolivianos, just over $7,000[USD]/per**. Relatives reject proposal: **"We don’t want your money, it’s blackmail”.** CIDH registers concern at Decree clause denying families appeal rights at U.N.

APDH [Asamblea Permanente de Derechos Humanos] Argentina, denounces Anez and conspirators. Charged with crimes against humanity and genocide. Invokes universal jurisdiction. **CARICOM solidarity vote is a blow to the OAS.** In Paraguay the broad left front Frente Guasu and University of Asuncion student union shut down Almagro visit.  Code Pink and coalition successfully disrupt and shut down Camacho talk. **"utter chaos in D.C.**

## ECONOMY

the creeping return of a failed model: foreign investment and 'export or die' economic growth. **Privatization of publicly owned BoA (Bolivian Airlines)** turned over to privately held Amazonas. Wave of privatisations publicly discussed. A flagrant power flex by so-called interim govt. Water and gas prices rising alleged El Alto.

## NEOFASCIST REALITY INVERTS ITSELF

**UJC claims victimhood as subjects of violence, humiliation, rights denial:** "Ni olvido ni perdon. Justicia". UJC promotes 'Ley de Juventud' to counter Garantias. **Murillo lies about massacres: 'not one bullet fired.'** Private **media amplifies lies-as-truth:** **demonstrators fired upon each other. Senkata action 'preventative' against terrorism.** 

**Salary bump to armed forces. Feted as heroes.** Santa Cruz Senator claims Crucenistas have been MAS victims of 'political persecution.' Proposes dropped charges and warrants brought against 2008 coup plotters and perpetrators. **Anez publicly commends Cochabamba (para) youth and civic groups for "peacefully restoring democracy"** to Bolivia. Anez announces intergovernmental **committee for "Victims of Political and Ideological Injustice of last 14 years".** Interim gov sacks all socialised media. Communications Minister issues **decree to "Recover Freedom of Expression in Bolivia".**

edit formatting, rolling updates
