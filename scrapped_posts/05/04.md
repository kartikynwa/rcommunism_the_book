Hello to All Comrades. 

While gathering sources and refining my Venezuela post, I came across a rehash of accusations against Chile's former president Salvador Allende. 

Allende was of course as you all may know, deposed in a violent coup by the dictator Augusto Pinochet, who was financed, armed and supported by the US. Who helped cover up his crimes for decades as well as assisted him in his belligerent right-wing foreign policy.

Allende was the first democratically elected Marxist-president in Latin America, and was therefore right in the firing line of the United States.

Due to the election of the fascist Jair Bolsonaro in Brazil, the life of Salvador Allende has been giving a renaissance of sorts. Bolsonaro wishes to "eradicate Marxism" From Brazil and he has begun by [essentially wiping out leftist history in Brazilian universities](https://twitter.com/castriotar/status/1055836519318122496)

It is now pertinent for us to preserve our history and the achievements and works of our comrades. 

According to my personal sources in Brazil, one of Mr. Bolsonaro's favorite targets in his private rants is Comrade Allende. He reiterates accusations that have been floating around for a while. 

Accusations That Comrade Allende was a Homophobe and an anti-semite. 

Now, far be it for Bolsonaro to actually care about either of those things, perhaps he wishes to use them to smear leftists, but that aside, it's up to us to right the intellectual wrongs here. 

The claims stem from [one book published by Victor Farías](https://www.telegraph.co.uk/news/worldnews/southamerica/chile/1489845/Allende-branded-a-fascist-and-anti-Semite.html), a Chilean historian, entitled: "Salvador Allende: Antisemitism and Euthanasia"

In this book, Farías claims that Allende in his graduation thesis, expressed anti-Semitic views and views such as proclaiming crime, mental illness and alcoholism as hereditary.

He later published another book containing claims that Allende worked with the Nazi party of Germany and garnered assistance from the Soviet Union. While the former of these claims is based on complete hearsay, and is the academic equivalent of the Da Vinci Code, the latter is based on documentation from East German Archives. 

To the claim that Allende worked with East Germany and the Soviet Union, we say: so what? Adherents of an ideology which proposes collective aid and thinking and working together....worked together?

Anyway, as mentioned, the whole of these claims by Farías all stem from the apparently right-wing views assumed by Allende in his doctoral thesis, rather than actual paper documentation showing Allende's support for Nazis or his agreement with any of the views.

It's simply assumed that because he apparently held these views in his thesis, clearly, he was a Nazi. 

First of all, Farías bases his book on the premise that the doctoral thesis has been kept hidden, perhaps to hide it from the world in some cover-up by the leftists.

This is simply not true. Anyone who wanted to see it could and [can ask for it at the University of Chile Medical School](http://www.ipsnews.net/2005/05/chile-book-smearing-allende-took-thesis-statements-out-of-context-says-former-adviser/)

In fact, [here's the thesis itself.](http://www.elclarin.cl/fpa/pdf/tesis_sag.pdf)

The reality is is that Mr. Farías misread the thesis in his zeal to attach something to Allende, and attributed quotes to Allende about specific medicinal, social and health issues that Allende did not at all produce. 

For instance, Mr. Farías' claims that Allende implied that mental illnesses, criminal behaviour, and alcoholism were hereditary.

The reality is that in his thesis Allende was merely quoting Italian criminologist [Cesare Lombroso](https://en.wikipedia.org/wiki/Cesare_Lombroso), whereas he himself was critical of these theories. 

[Here is Lombroso's work](http://www.elclarin.cl/fpa/pdf/lombroso.pdf), where the quotes that Farias attributes to Allende are present.

Allende took various statements from Lombroso, and discussed them in his thesis. Remember, Allende was [a medical student](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1448142/)


Whereas Farías was presenting it as if Allende himself had written these statements.

Such was the audacity of these accusations that the Allende Foundation of Spain in April 2006 [filed an anti-libel claim against Farías and his publisher in the Court of Justice of Madrid.](http://www.elclarin.cl/fpa/pdf/p_180406.pdf)

Unfortunately, Farias' version of Allende's thesis, implying he was an anti-semite and had unscientific and un-leftist views about crime and genetics was widely distributed in the media. 

The [Daily Telegraph](https://www.telegraph.co.uk/news/worldnews/southamerica/chile/1489845/Allende-branded-a-fascist-and-anti-Semite.html) for instance, said that:

> "**Allende**....**wrote**: 'The Hebrews are characterized by certain types of crime: fraud, deceit, slander and above all usury. These facts permits the supposition that race plays a role in crime.' Among the Arabs, he wrote, were some industrious tribes but 'most are adventurers, thoughtless and lazy with a tendency to theft'

In Allende's dissertation, these two sentences do not occur together, yet the Telegraph presented them as such. They occur as part of a summary of Cesare Lombroso's views on different "tribes", "races" and "nations" being prone to different types of crime; the latter is misquoted.

Allende's passage summarises an explanation of Lombroso's teories about the Jews reads "The Hebrews are characterized by certain types of crime: fraud, deceit, defamation and, above all, usury. On the other hand, murders and crimes of passion are the exception." 

After recounting Lombroso's views, Allende writes, "We lack precise data to demonstrate this influence [that race influences crime] in the civilized world." 

The  Telegraph also, perhaps purposefully, misquotes Allende.

Hispassage about Arabs reads:

> "Lombroso states relates that there are tribes more or less given to crime....Among the Arabs there are some honored and hardworking tribes, and others who are adventurers, thoughtless and lazy with a tendency to theft." [page 114]

There is no statement that the latter applies to "most" Arabs, which the Telegraph simply adds to convey their point. Allende is simply summarising Lombroso's points. In the end of his summary, Allende rejects Lombroso's points by stating that there is no evidence for such claims. 

Another individual whose ideas Allende discusses is the endocrinologist Nicolas Pende, who was a member of the fascist party of Italy and was said to have helped write the racial doctrines of the party. Farias claims that Allende supported Pende's work.

But Allende concludes his summary of Pende with: "The ideas previously discussed in relation to the neurovegetative system and endocrinology should be taken with serene and equitable criteria" [Page 96]

Allende himself describes the theories of the “endocrinological school” , that propositioned by Pende, as “insufficient, simplistic and one-sided” 

Allende concludes his dissertation by emphasising his belief in free will and human conscience, which of course vastly differs from the supposed endorsement of theories of genetic predisposition attributed to him by Farías.

Farías further claims that Allende had tried to implement 'his' ideas about heredity during his period as Health Minister from 1939 to 1941, and also received help from German Nazis to draft a bill mandating forced sterilization of alcoholics. Farias also claims Allende was bribed by the Nazi foreign minister Joachim von Ribbentrop.

The President Allende Foundation [challenged Farias in the Court of Justice - Madrid](http://www.elclarin.cl/fpa/pdf/p_180406.pdf) to prove the existence of such a bill and Allende's links to it, as well as the claim about collaboration with nazis.

The claims of pseudoscience are ridiculous, as Allende is dicussing them and dismissing them in a scientific manner, as befits a student writing a thesis. 

Particularly one who [studied under](http://ije.oxfordjournals.org/content/34/4/739.full) one of the engineers of modern social medicine, [Max Westenhofer](https://en.wikipedia.org/wiki/Max_Westenhofer)

The claims of anti-semitism are also profound nonsense. 

Allende's mother, Laura Gossens Uribe, was of Jewish descent and thus, Allende was himself Jewish, although a professed atheist and this very own Jewish ancestry of his was often used by his political detractors against him.

The Chilean diplomat and Nazi Miguel Serrano for instance, who was [dismissed by Allende](https://www.reforma.com/aplicacioneslibre/preacceso/articulo/default.aspx?id=1254679&urlredirect=https://www.reforma.com/aplicaciones/articulo/default.aspx?id=1254679), and who mentored the PYL movement that laid the foundations for the coup [often spoke about Allende’s “Jewishness” or his supposed “Judeo-Bolshevik” agenda.](http://repositorio.uchile.cl/bitstream/handle/2250/123091/judeofobia-en-Chile-durante-la-decada-del-ochenta-la-obra-de-Miguel-Serrano.pdf?sequence=1)

Following Kristallnacht, Allende and other Chilean political figures [signed and sent a letter of condemnation to Adolf Hitler](http://www.elclarin.cl/fpa/pdf/p_020605.pdf)

One of the strangest claims that Farias makes is that Allende sheltered Nazis, particularly Walter Rauff.

This is ahistorical nonsense and it has no basis. The only times the figure of Allende and Rauff ever cross is when Allende hears from his polticial advisors that Simon Weisenthal, famed Nazi hunter has sent him a letter requesting the extradition of one Walter Rauff, an inquiry by Allende finds that in December 1962, a nazi named Walter Rauff was arrested by Chilean authorities after Germany requested his extradition, but he was freed by a Chilean Supreme Court decision five months later in 1963 on the grounds that his crimes had been committed too long ago. 

Salvador Allende was elected Chilean president in 1970. 8 years after the arrest.

He wrote a letter to Simon Wiesenthal, [stating that he could not reverse the Supreme Court's 1963 decision](http://www.haaretz.com/weekend/magazine/in-the-service-of-the-jewish-state-1.216923)

Allende suggested that Wiesenthal request the Supreme Court of Chile to extradite him to Germany. They had an amicable correspondence about it which [you can read in full here](http://www.elclarin.cl/fpa/pdf/p_280505.pdf)

That's it. That's the only time that Salvador Allende ever crossed paths with the mere name of Walter Rauff, let alone his physical manifestation.

Here's how Weisenthal [described it](https://web.stanford.edu/group/wais/ztopics/week050105/chile_050501_salvadorallendethesis.htm):

> "Eight years later just that happened: the Socialist Salvador Allende became head of state. On 21 August I handed over to the Chilean ambassador in Vienna, professor Benadava, a letter to Allende, drawing his attention to the Rauff case. Allende relied very cordially but pointed to the difficulty of reopening a case when the Supreme Court had already handed down a judgment. I requested Allende to examine the possibility of having Rauff, who was not yet a Chilean citizen, deported: we might be able to proceed against him in a country with a more favorable legislation. But before Allende could answer my second letter there was a coup and Allende lost his life". 

What's interesting is Farias' silence on the real identity of those who hid Rauff and denied his extradition.

Particularly one Augusto Pinochet.

In fact, Under Pinochet, Rauff [served as an advisor to the Chilean secret police, DINA.](https://archive.org/stream/RauffWalter/RAUFF%2C%20WALTER_0110_djvu.txt)

Pinochet resisted all calls for his extradition to stand trial in either West Germany or Israel. 

The [last request for extradition was made by Nazi hunter Beate Klarsfeld in 1983](https://www.bbc.com/news/world-europe-15069333)

This was rejected by the Pinochet regime, which stated that Rauff had been a peaceful Chilean citizen for over twenty years.

Klarsfield organised protests against the decision and was [jailed for doing so](http://www.haaretz.com/weekend/magazine/in-the-service-of-the-jewish-state-1.216923)

After that, in 1984, Israel's DGMOFA David Kimche requested to Jaime Del Valle, the minister of foreign affairs of Chile, to extradite Rauff. [This too was refused](https://www.nytimes.com/1984/02/02/world/around-the-world-chile-refuses-to-expel-former-nazi-officer.html)


It is our duty to defend our comrades from falsehoods, lies and baseless accusations. 
