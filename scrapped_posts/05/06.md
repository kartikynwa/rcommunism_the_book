*"Everything for everyone, nothing for ourselves." - Zapatista Slogan*

## Introduction

Along with the Naxalite movement of India, the Zapatista Army of National Liberation (more commonly known as the EZLN, or simply the Zapatistas) is arguably the most significant revolutionary socialist formation in the world today. Since launching their insurrection on January 1st, 1994, the EZLN have served as an inspiration to leftists and indigenous people all over the world. In honor of the recent announcement that the EZLN are formally expanding their territory (source below), we should take this opportunity to discuss the Zapatistas, their achievements, and the lessons that they can teach us all. We can also study the pros and cons of their particular approach to revolutionary socialism, which focuses much more on community autonomy and direct democracy, rather than establishing a dictatorship of the proletariat.

As always, sources will be listed at the end, and I will mention which I am using when I quote.

## Economic System and Living Standards

Before the formation of the EZLN, the people of Chiapas were ruthlessly exploited by the capitalist system that existed in their region. In 2015, Subcomandante Insurgente Moisés remarked:

>Before the Zapatista Army for National Liberation was created, we indigenous from Chiapas didn’t exist for the capitalist system; we weren’t people to it; we weren’t human. We didn’t even exist as trash for it. And we imagine that’s how it was for the other indigenous brothers and sisters in the rest of our country. And that’s how we imagine it is in any country where indigenous people exist... No one knew about highways, no one knew that there were things called clinics and hospitals, much less schools, or classrooms for education. There were never any health campaigns, programs, grants, nothing. We were forgotten.

Since establishing their autonomous communities, the EZLN have radically transformed the economic system in the region. They have established a system based on cooperatives and collectively owned landholdings. Anarchist writer Hilary Klein writes:

>In addition to health care and education, the Zapatistas have also constructed an economic infrastructure designed to address the high level of poverty in their communities. Often called a 'solidarity economy', the Zapatistas' autonomous economy offers a grassroots alternative to global capitalism \[...\] Economic cooperatives generate resources that are invested back into the community. Because of the gendered division of labor, there are often men's and women's cooperatives. Men, for example, have coffee or cattle cooperatives, whereas women have artisan cooperatives, chicken-raising cooperatives, and collective vegetable gardens. Cooperative stores provide merchandise for community members at reasonable prices while also generating income. Money raised by the cooperatives is used to cover shared expenses, for example when the community's representatives travel to a regional meeting.

This system has had some success in increasing incomes for the region; however, the EZLN's refusal to take state power has limited the extent of what they can achieve. Despite the rich natural resources of the region, the EZLN have chosen to remain primarily agrarian, refraining from attempts to extract and utilize these resources (for which they have received criticism from many Marxists, such as Louis Proyect). This is tied both to the refusal to take state power, and the desire to preserve traditional indigenous lifestyles (a major aspect of the Zapatista rebellion).

In short, while the EZLN have succeeded in breaking free from the chains of international capital and establishing a system based on mutual aid and common ownership, they have not established a fully-developed socialist economy. Whether they will do so in the future is yet to be seen. The real achievements of the EZLN are in the areas of health, education, and social policy, as we shall shortly see.

## Healthcare in Zapatista Territories

Before the initial uprising, the people in present-day EZLN territory had very little access to healthcare, and they lived with truly wretched health conditions. According to the National University of Ireland, Cork:

>This neglect of the provision of adequate healthcare services in Chiapas became one of the major issues that provoked the Zapatista movement into vocalizing its concerns... This vision quickly drew the Zapatista community together to begin the construction of a healthcare system centered on their individual needs, customs and culture. Autonomous healthcare was born and the concept rapidly became popular amongst all Zapatista communities.

Since the EZLN uprising of 1994, the Zapatistas have made great strides in healthcare in the territories under their control:

>It is clear that the efforts invested by the Zapatistas in establishing a fully independent healthcare system using limited resources have been rewarded by the significant health benefits achieved throughout hundreds of Zapatista communities. The Zapatistas have witnessed improvements in women's and children's health. They have also seen the health benefits of improved hygiene as a result of the role of the health promoter and their role in educating the community. Autonomous healthcare has ignited a sense of purpose in the hearts and minds of community inhabitants as they confidently tackle the health problems of their villages. In essence, autonomous healthcare has undoubtedly brought lasting health benefits into the world of the Zapatistas.

The World Health Organization states:

>There are currently some 200 community health houses and 25 autonomous regional clinics, some of which have already been in operation for 10 years, and a dental clinic... If we bear in mind that almost all the medical facilities have been built in places where none existed before, it is easy to appreciate the significance of achieving this objective thanks to an effort by the community.

The health of women has improved dramatically in Zapatista territories:

>Autonomous healthcare is having a positive effect on women's and children's health. In regions where there were previously significantly high rates of death during childbirth, there has now been a period of eight years or more where no maternal deaths have been recorded. With the greater availability of local midwives and the basic medical knowledge provided by the health promoter, Zapatista women are now safely giving birth in their own communities, with a dramatically reduced risk of death. In addition to this, cancer screenings and sexual health examinations take place more frequently, further improving the quality of women's lives

The health of children has also been greatly improved. The National University of Ireland, Cork states:

>As with women, the improvement in children's health is also noteworthy. Zapatistas now actively prioritize children's vaccinations ensuring that as many children as possible receive the necessary immunization. Health promoters are also trained to detect the symptoms associated with sepsis and jaundice in newborn infants and through the use of visual media, foreign organisations are teaching health promoters to identify and treat such illnesses.

Zapatista communities have higher rates of vaccination than pro-government communities in Chiapas:

>With the wider availability of autonomous healthcare, 84% of Zapatista communities receive important vaccinations against diseases such as malaria. In pro-government communities that figure stands at only 75%, meaning that fewer of these community inhabitants have access to necessary vaccines despite the promises of State healthcare.

In addition, the Zapatistas have significantly reduced the levels of infectious disease in their communities:

>A greater disparity is apparent between pro-government communities and Zapatista villages with regard to the treatment of tuberculosis. Currently, 32% of Zapatista inhabitants suffer TB while in larger portions of pro-government communities, a remarkable 84% continue to experience this respiratory infection.

The Zapatistas have also greatly improved hygiene and sanitation infrastructure in the territory under their control:

>Currently, 74% of Zapatista communities now have access to toilet facilities and this has lead to a vast improvement in personal hygiene. Only 54% of pro-government communities can claim access to toilet facilities in their homes. This provides clear evidence of the positive impact the health promoter has in educating communities around health and hygiene. It also suggests that the Zapatista communities have an appetite for knowledge and are willing to learn all that is necessary to protect their health.

The Zapatistas have even managed to virtually eliminate alcohol consumption in their communities, which has had a massively positive impact on public health:

>Proclaimed as one of the greatest health achievements for the Zapatista movement, the eradication of both the manufacture and consumption of alcohol has significantly improved the health of all Zapatistas. This no-tolerance policy is directly linked to the reduction in many illnesses and infections including “úlcera, cirrhosis, desnutrición y heridas con machete”.

Overall, the Zapatistas have made a number of tremendous strides in healthcare in their communities. The National University of Ireland, Cork remarks:

>It is clear, as these figures demonstrate, that Zapatista healthcare has a profound impact on the quality of the lives of the many indigenous who attend autonomous healthcare services regularly... Zapatista autonomous healthcare has proven successful, in many respects, in reducing disease and promoting community independence. This approach to healthcare empowers communities to access resources and knowledge which have opened up unparalleled opportunities for survival amongst remote and isolated indigenous communities.

The World Health Organization notes:

>This is a model which has proved able to have an impact on what one can call the primary level of health care, acting within an organized health structure and as part of a political project. Seen from that viewpoint, it has been able to insert into the scenario a methodology that the National Health Service has been unable or unwilling to develop.

These achievements have also succeeded in instilling a sense of dignity and pride into these communities, which had previously been so badly exploited. The National University of Ireland, Cork states:

>As a result of autonomous healthcare, communities are now in control of their healthcare services and together they decide on local healthcare policy and the appointment of a suitable health promoter. Therefore, it is only natural that these communities develop a self-confidence and empowerment not felt before the widespread introduction of autonomous healthcare.

The healthcare achievements in Zapatista territories stand as some of the revolution's greatest accomplishments.

## Education in Zapatista Territories

In addition to healthcare, the EZLN have also greatly improved the provision of education in their communities. Before the uprising, education was in a very poor state. The National University of Ireland, Cork notes:

>Education in Chiapas was of a low standard and many of the statistics that follow substantiate this claim. According to Rovira (2000), 30% of children in Chiapas did not attend school while 6.5% of the Chiapan population were considered illiterate. Other critics, however, placed the illiteracy rate in Chiapas much higher, arguing that roughly 18% of the state's population lacked any form of basic education... In addition, further statistics released highlight the under-performance of those who have access to basic schooling. Only 10% of indigenous children complete primary level education in Chiapas. In direct contrast, 54% of children nationally attain the same standard of education

As the above statistics show, indigenous children were at a particular disadvantage. This is especially true because most teachers did not speak an indigenous language, thus impeding the ability of indigenous children to learn. State schools have often been accused of shaming indigenous children, with disastrous results:

>Families feel anger and frustration because State education is “shaming them for being indigenous” and State-employed teachers accuse them of speaking “una lengua de perros”, a demeaning reference to the children's use of native indigenous languages. According to Castellanos (2014), alcohol abuse is prevalent in many pro-government communities and such high levels of consumption are linked to significantly high rates of suicide amongst young indigenous teenagers. It is claimed that many indigenous young people in these communities resort to excessive levels of alcohol consumption as a means of coping with the discrimination they experience in school. It is estimated that, on average, six attempted suicides take place each month amongst this cohort of young indigenous people living in pro-government communities.

By contrast, Zapatista schools have protected and assisted the indigenous:

>Many observers of the Zapatista movement have noted that the indigenous uprising of 1994 brought about a momentous transformation and “re-signification of what it means to be indigenous” in modern-day Mexico. For pro-Zapatista communities these “sudden cultural changes” were experienced within the protective political and social framework of the Zapatista movement. Today, autonomous education encourages communities to celebrate their indigenous identities and welcomes the “re-evaluation of their culture \[and\] their language”... it is widely accepted that, through autonomous education, the Zapatista rebels have “sheltered Zapatista youth from the social changes” which have so severely impacted on those of whom are living outside the protective frameworks of the Zapatista movement.

The Zapatistas have greatly expanded the educational system in their territories, and have even begun to set up a second-tier to their educational system:

>Over the years, autonomous education has expanded its reach throughout the region and, as a result, the rebels have started to develop a second tier in the education system. This slow and steady commitment to advance their education system demonstrates the important and respected position that education holds in the wider project of Zapatista autonomy.

While the Zapatista system remains focused principally on primary education, it has nonetheless succeeded in greatly improving access to education in the region. The National University of Ireland, Cork reports:

>While primary education is available in all Zapatista communities, there are only five secondary schools in operation across the region, one constructed in each *Caracol*. Figures released in 2001 indicate that 37% of all Zapatista students are now steadily progressing towards second level education. Nevertheless, according to Barmeyer's (2008) observations, primary school remains the only educational service that is available in all indigenous communities in the autonomous territory. However, despite its rudimentary infrastructure and under-resourced classrooms, autonomous education is a finely tuned system designed precisely to deliver the objectives the Zapatistas expected of it.

The Zapatista educational system is not only focused on classroom learning; it also helps local young people to learn the necessary skills to run and sustain a community:

>Zapatista students regularly attend lessons in agriculture and environmental sustainability, learning about viable farming techniques which help protect the environment and assist in maximizing local farmers' limited agricultural resources. Nowadays, young farmers are learning to plant vines in their fields as a way of suffocating and reducing the spread of weeds on their lands. It is a practice that returns nutrients to the soil and makes ready the fields for consecutive agricultural seasons. Prior to the introduction of this unique farming method, many farmers failed to protect their fields and often burned weeds, thus eliminating all possibility of “enriching the ground with nutrients and nitrogen”. Education of this kind is a means of safeguarding and securing the growth of vital crops which are important for supporting community inhabitants and their local economies.

All of this information demonstrates that the Zapatistas have greatly improved educational access in their communities.

## Women's Rights and Anti-Racism in Zapatista Communities

Another important advance made by the Zapatistas is the improvement in gender relations in their communities. A major factor in this has been the banning of alcohol, which has greatly reduced levels of domestic violence. The National University of Ireland, Cork reports:

>Women were often bartered in return for alcohol and social relations between the genders often amounted to no more than men treating women like “domestic animals”. Now, with their complete rejection of alcohol, the Zapatistas can claim a significant improvement in gender relations, with women now challenging “the traditional order” of gender roles in communities. Removing alcohol from Zapatista life has both improved health and “soften\[ed\] human relations” considerably.

Democratic community participation has also played a role, as people in each community are free to openly discuss problems of gender relations and sexual violence:

>In a community healthcare debate witnessed by the author, Villarreal (2007) asserts that the issue of sexual health dominated the discussions at a community assembly. This demonstrates the liberty communities have in tackling the health problems that confront their village and the freedom to do so on their terms.

The EZLN have taken an active approach to fighting patriarchy. In 2018, the Zapatistas organized an event entirely by, and for, women. Waging Nonviolence reports:

>“What we wanted was to meet many women,” said Commander Jenny, who coordinated the event. “We thought that only a few women were going to come, so we are very happy to see how many of you have joined us here.” Although only her eyes were visible, a smile was detectable behind her black balaclava. “It has been hard work, but we are very pleased to see that there are many other women who are fighting patriarchy.” \[...\] The event was not only an opportunity to create educational or professional networks, but also a space to consider one’s health and well-being as a woman in the fight for justice. There were activities ranging from workshops, discussion panels and movie screenings to theater performances, art exhibitions and sports events, including basketball and soccer matches. Themes included gender violence, self-defense, self-care, sexism in the media, sexual rights, health and education, misogyny and childhood, discrimination against indigenous LGBTQ communities, women environmental rights defenders, and decolonization. All of the activities were led and held by women, and all of them were aimed at generating consciousness of gender inequality or the restoration of women’s self-confidence and autonomy.

The EZLN connect their fight against patriarchy and racism to the struggle against capitalism:

>“Capitalism is not only colonial, it is also patriarchal and racist,” said Fernanda Esquivel, a  20-year-old student from Guadalajara. “To come here and see that the Zapatistas are still resisting and have resisted for so many years is a huge inspiration for me. Being with so many women and feeling united also makes me feel hopeful about really creating a change. In academia there is nothing that can show you what it is like to come here, and to feel and share these experiences in practice.”

The Zapatistas have implemented their Revolutionary Law to fight against gender oppression and other forms bigotry:

>Many followers of the Zapatista revolution were not aware of the key elements that formed the movement before going public in 1994. Undeniably, one of the key characteristics that shaped the movement was the “Women’s Revolutionary Law,” passed by the Zapatista committees in 1992.  
>  
>For Sylvia Marcos, a sociologist and expert on indigenous movements across the Americas, the emphasis on women’s rights is a defining factor for the organization. Furthermore, she indicates that these rights were claimed not solely for women as individuals, but were “fully linked and interwoven with collective rights.”

Overall, it is clear that the EZLN have taken a firm stance against patriarchy, racism, and other forms of discrimination.

## Successful Anarchists?

Finally, it should be noted that, despite the excited claims of numerous anarchists, the Zapatistas are *not* an anarchist group. In fact, they explicitly reject the label (source below). In addition, they use imagery from the Cuban Revolution (particularly Che Guevara), and Marxist concepts appear frequently in the writings of major figures in the Zapatista movement, including the group's *de facto* spokesman, Subcomandante Marcos (now known as Subcomandante Galeano).

## Conclusion

The EZLN have managed numerous great achievements since their uprising in 1994; the recent expansion of their territory lends hope that their revolution will grow and improve. While they have not yet succeeded in establishing developed socialism, their existing achievements are already enough to guarantee them a place in the history of great proletarian and anti-imperialist revolutionary movements.

## Sources

* [The Nation | The Zapatista Revolution is Not Over](https://www.thenation.com/article/zapatista-chiapas-caracoles/)
* [Enlace Zapatista | Political Economy from the Perspective of the Communities: Words of Subcomandante Insurgente Moises](http://enlacezapatista.ezln.org.mx/2015/05/24/political-economy-from-the-perspective-of-the-communities-i-words-of-subcomandante-insurgente-moises-may-4-2015/)
* [National University of Ireland, Cork | Understanding Zapatista Autonomy: An Analysis of Healthcare and Education](https://www.academia.edu/33163010/Understanding_Zapatista_Autonomy_An_Analysis_of_Healthcare_and_Education)
* [World Health Organisation (WHO) | Health and Autonomy: The Case of Chiapas](https://www.who.int/social_determinants/resources/csdh_media/autonomy_mexico_2007_en.pdf)
* [Waging Nonviolence | Zapatista Women Inspire Fight Against Patriarchy](https://wagingnonviolence.org/2018/04/zapatista-women-inspire-fight-against-patriarchy/)
* [Indigenous Anarchist Federation | Zapatista Response to "The EZLN is NOT Anarchist"](https://iaf-fai.org/2019/05/05/a-zapatista-response-to-the-ezln-is-not-anarchist/)
